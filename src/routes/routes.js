import Home from '../pages/Home'
import ListStudents from '../components/ListStudents'
import NotFound from '../pages/NotFound'
import RegisterStudent from '../components/RegisterStudent'
import SearchStudent from '../components/SearchStudent'
import EditStudent from '../components/EditStudent'

export default [
    {
        path: '/',
        component: Home,
        redirect: '/students'
    },
    {
        path: '/students',
        component: ListStudents,
    },
    {
        path: '/registerStudent',
        component: RegisterStudent
    },
    {
        path: '/searchStudent',
        component: SearchStudent
    },
    {
        path: '/editStudent/:id',
        component: EditStudent
    },
    {
        path: '*',
        component: NotFound
    }
]